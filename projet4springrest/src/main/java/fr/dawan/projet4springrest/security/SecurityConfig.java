package fr.dawan.projet4springrest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfig {
    
    @Autowired
    private UserDetailsService userService;
    
    @Bean
    AuthenticationProvider provider() {
        AppAuthProvider provider=new AppAuthProvider();
        provider.setUserDetailsService(userService);
        return provider;
    }
    
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http,AuthenticationProvider provider) throws Exception {
        http
        .csrf(csrf->csrf.disable())
        .authenticationProvider(provider)
        .authorizeHttpRequests(auth -> auth
              .requestMatchers(HttpMethod.POST).hasAnyAuthority("SUPER","WRITE")
              .requestMatchers(HttpMethod.DELETE).hasAnyAuthority("SUPER","DELETE")
              .requestMatchers(HttpMethod.PUT).hasAnyAuthority("SUPER","WRITE")   
              .requestMatchers(HttpMethod.GET).hasAnyAuthority("SUPER","READ")
                .anyRequest().permitAll())
                .httpBasic();
        return http.build();
    }
    
    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
