package fr.dawan.projet4springrest.entities;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
@Entity
public class Autorities implements GrantedAuthority {

    private static final long serialVersionUID = 1L;
    @Id
    private String authority;

    @Override
    public String getAuthority() {
         return authority;
    }

    public void setAuthority(String authority) {
        this.authority = authority;
    }

}
