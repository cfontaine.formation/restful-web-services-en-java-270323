package fr.dawan.projet4springrest.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {

    @Value("${message}")
    private String message;

    @GetMapping("/hello")
    public String helloWorld() {
        return message;
    }
}
