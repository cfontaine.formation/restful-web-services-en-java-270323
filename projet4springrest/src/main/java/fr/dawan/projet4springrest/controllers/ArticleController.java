package fr.dawan.projet4springrest.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.projet4springrest.entities.Article;
import fr.dawan.projet4springrest.repositories.ArticleRepository;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/api/articles")
@Tag(name = "Articles", description = "L'api des articles")
public class ArticleController {

    @Autowired
    private ArticleRepository repositoryArticle;

    @ResponseStatus(code = HttpStatus.I_AM_A_TEAPOT)
    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public List<Article> getAllArticles() {
        return repositoryArticle.findAll();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { "page", "size" })
    public Page<Article> getAllArticlesPage(Pageable page) {
        return repositoryArticle.findAll(page);
    }

    @Operation(summary = "Trouver les articles à partir de leurs Id", description = "retourne un article", tags = "Articles")
    @ApiResponses({
            @ApiResponse(responseCode = "200", description = "opération réussit", content = @Content(schema = @Schema(implementation = Article.class))),
            @ApiResponse(responseCode = "404", description = "Article non trouvé", content = @Content(schema = @Schema(implementation = String.class))) })
    @GetMapping(value = "/{id:[0-9]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Article getArticleById(
            @Parameter(description = "L'ide de l'article", required = true, allowEmptyValue = false) @PathVariable long id) {
        return repositoryArticle.findById(id).get();
    }

    @GetMapping(value = "/{description:^[a-zA-Z][a-zA-Z0-9 ]+$}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticleByDescription(@PathVariable String description) {
        return repositoryArticle.findByDescriptionLikeIgnoreCase("%"+description+"%");
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteArticle(@PathVariable long id) {
        try {
            repositoryArticle.deleteById(id);
            return new ResponseEntity<>("L'article id=" + id + "est supprimé", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("L'article n'existe pas", HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Article addArticle(@RequestBody Article article) {
        return repositoryArticle.saveAndFlush(article);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Article updateArticle(@PathVariable long id, @RequestBody Article article) {
        article.setId(id);
        return repositoryArticle.saveAndFlush(article);
    }

    @GetMapping("exception/io")
    public void genIOException() throws IOException {
        throw new IOException("Le traitement a échoué");
    }

    @GetMapping("exception/sql")
    public void genSqlException() throws IOException, SQLException {
        throw new SQLException("La requete a échoué");
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> gestionIoException(IOException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
