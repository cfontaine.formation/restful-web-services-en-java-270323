package fr.dawan.projet4springrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projet4springrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Projet4springrestApplication.class, args);
//		SpringApplication app=new SpringApplication(Projet4springrestApplication.class);
//		app.setAddCommandLineProperties(false);
//		app.run(args);
	  }
}
