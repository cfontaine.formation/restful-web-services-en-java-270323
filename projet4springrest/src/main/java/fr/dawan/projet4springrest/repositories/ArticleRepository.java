package fr.dawan.projet4springrest.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.projet4springrest.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<Article> findByDescriptionLikeIgnoreCase(String model);

}
