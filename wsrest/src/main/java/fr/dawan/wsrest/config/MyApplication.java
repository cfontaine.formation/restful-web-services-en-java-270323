package fr.dawan.wsrest.config;

import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.core.util.JacksonFeature;

import jakarta.ws.rs.ApplicationPath;

@ApplicationPath("/api")
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        packages("fr.dawan.wsrest.ws");

        register(JacksonFeature.class);

        register(MultiPartFeature.class);
    }

}
