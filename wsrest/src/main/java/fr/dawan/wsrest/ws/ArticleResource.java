package fr.dawan.wsrest.ws;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.glassfish.jersey.media.multipart.FormDataContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataParam;

import fr.dawan.wsrest.entities.Article;
import fr.dawan.wsrest.repositories.FakeArticleRepository;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

@Path("/articles")
public class ArticleResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        return Response.ok().entity(FakeArticleRepository.findAll()).build();
    }

    @GET
    @Path("/{id:[0-9]+}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getById(@PathParam("id") long id) {
        Article a = FakeArticleRepository.findById(id);
        if (a != null) {
            return Response.ok().entity(a).build();
        } else {
            return Response.status(404).build();
        }
    }

    @GET
    @Path("/{description:^[a-zA-Z][a-zA-Z0-9 ]+$}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getArticleByDescription(@PathParam("description") String description) {
        List<Article> articles = FakeArticleRepository.findByDescription(description);
        if (articles.isEmpty()) {
            return Response.status(404).build();
        } else {
            return Response.ok().entity(articles).build();
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response create(Article a) {
        return Response.status(201).entity(FakeArticleRepository.saveOrUpdate(a)).build();
    }

    @PUT
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response update(@PathParam("id") long id, Article a) {
        a.setId(id);
        return Response.ok().entity(FakeArticleRepository.saveOrUpdate(a)).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") long id) {
        try {
            return Response.ok().entity("L'article id=" + id + " a été suprimé").build();
        } catch (Exception e) {
            return Response.status(404).entity("L'article  id=" + id + "n'existe pas").build();
        }
    }

    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    public Response upload(@FormDataParam("file") InputStream stream,
            @FormDataParam("file") FormDataContentDisposition fileDetails) {
        try {
            Files.copy(stream, Paths.get("c:/Tools/" + fileDetails.getFileName()), StandardCopyOption.REPLACE_EXISTING);
            return Response.ok().entity("Upload effectué").build();
        } catch (IOException e) {
            return Response.status(400).entity("Erreur upload").build();
        }
    }
}
