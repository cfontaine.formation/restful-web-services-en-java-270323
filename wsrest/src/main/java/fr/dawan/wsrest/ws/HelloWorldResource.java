package fr.dawan.wsrest.ws;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;

@Path("/")
public class HelloWorldResource {

    @GET
    @Path("helloworld")
    public String helloWorld() {
        return "Hello World API !!!";
    }

}
