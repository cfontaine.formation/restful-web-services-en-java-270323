package fr.dawan.projet3springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;

import fr.dawan.projet3springcore.beans.ArticleRepository;
import fr.dawan.projet3springcore.beans.ArticleService;

public class App 
{
    public static void main( String[] args )
    {
        // Création du conteneur d'ioc
       ApplicationContext ctx=new AnnotationConfigApplicationContext(AppConf.class);
       
       // getBean -> permet de récupérer les instances des beans depuis le conteneur
       System.out.println("-----------------------------");
       ArticleRepository r1=ctx.getBean("repository1",ArticleRepository.class);
       System.out.println(r1);
     
       ArticleRepository r2=ctx.getBean("repository2",ArticleRepository.class);
       System.out.println(r2);

       // Scope:
       // Singleton -> Pour un singleton (ex: service1), getBean("service1") retourne toujours
       // la même instance
       // Prototype -> Pour un prototype (on décommente @Scope("prototype") dans Article service) getBean("service1") retourne une
       // nouvelle instance à chaque appel correspond à un new
       ArticleService s1=ctx.getBean("service1",ArticleService.class);
       System.out.println(s1);
       ArticleService s1bis=ctx.getBean("service1",ArticleService.class);
       System.out.println(s1bis);

       System.out.println("-----------------------------"); 
       // Fermeture du conteneur
       ((AbstractApplicationContext) ctx).close();
    }
}
