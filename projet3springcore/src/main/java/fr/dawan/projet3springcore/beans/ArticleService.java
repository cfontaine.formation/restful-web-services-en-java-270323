package fr.dawan.projet3springcore.beans;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service(value = "service1")
//@Scope("prototype")
//@Lazy
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;

    // @Autowired -> injection automatique de dépendence
    // une bean de type ArticleRepository va être recherché de le conteneur d'injection de dépendence
    // et injecté dans repo
    // Si plusieurs bean sont trouvés, il y a une ambiguité => exception
    // @Qualifier -> permet de levé l'ambiguité
    @Autowired // 1 - @Autowired peut être placé sur la variable d'intance (appel du contructeur par défaut, utilisation de la réflexion pour réaliser l'injection)
    @Qualifier("repository2")
    private ArticleRepository repo;

    public ArticleService() {
        System.out.println("Constructeur defaut");
    }

    // 2 - @Autowired peut être placé sur un contructeur (appel et utilisation du contructeur pour réaliser l'injection)
    // s'il n'y a pas qu'un seul constructeur @Autowired est implicite
    public ArticleService(ArticleRepository /*@Qualifier*/ repo) {
        this.repo = repo;
        System.out.println("Constructeur");
    }

    public ArticleRepository getRepo() {
        return repo;
    }

    // 3 - @Autowired peut être placé sur le setter (appel du contructeur par défaut, utilisation du setter pour réaliser l'injection)
    // @Autowwired
    public void setRepo(ArticleRepository /*@Qualifier*/repo) {
        System.out.println("setter");
        this.repo = repo;
    }

    @Override
    public String toString() {
        return "ArticleService [repo=" + repo + ", hashCode()=" + hashCode() + "]";
    }

}
