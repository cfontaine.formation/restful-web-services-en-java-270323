package fr.dawan.projet3springcore.beans;

import java.io.Serializable;

import org.springframework.stereotype.Repository;

// Avec les annotations @Component, @Repository, @Service, @Controller
// un bean sera créée et géré par le conteneur d'ioc de spring
@Repository("repository1") // on peut nommer le bean (repository1), sinon il aura le nom de la classe par défaut
public class ArticleRepository implements Serializable {

    private static final long serialVersionUID = 1L;

    private String data;

    public ArticleRepository() {

    }

    public ArticleRepository(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ArticleRepository [data=" + data + ", toString()=" + super.toString() + "]";
    }

}
