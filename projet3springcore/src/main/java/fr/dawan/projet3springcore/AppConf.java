package fr.dawan.projet3springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.projet3springcore.beans.ArticleRepository;

@Configuration // @Configuration => classe de configuration
@ComponentScan(basePackages = "fr.dawan.projet3springcore") // @Component, @Repository, @Controller,@Service
public class AppConf {

    // Déclarer un bean une méthode annotée avec @Bean
    // Le type de retour est le type du bean, le nom du bean et le nom de la méthode
    @Bean
    ArticleRepository repository2() {
        return new ArticleRepository();
    }

    // Le paramètre repository2 de la méthode permet d'indiquer que le bean dépend
    // du Bean qui a pour nom repository2
//    @Bean
//    ArticleService service2(ArticleRepository repository2) {
//        return new ArticleService(repository2);
//    }

}
