package fr.dawan.projet1xmljson;

import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;

import fr.dawan.projet1xmljson.entities.Produit;
import fr.dawan.projet1xmljson.tools.JsonTools;

public class Main6JacksonJson {

    public static void main(String[] args) {
        Produit p = new Produit("Cable HDMI", 15.0, LocalDate.of(2020, 10, 20));
        p.setId(1L);
        try (FileWriter fw = new FileWriter("produit.json")) {
            JsonTools.toJson(fw, p);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try (FileReader fr = new FileReader("produit.json")) {
            System.out.println(JsonTools.fromJson(fr, Produit.class));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
