package fr.dawan.projet1xmljson;

import java.io.FileInputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import fr.dawan.projet1xmljson.entities.Contact;

public class Main3StaxXML {

    public static void main(String[] args) {
        List<Contact> contacts = new ArrayList<>();
        try {
            XMLInputFactory factory = XMLInputFactory.newInstance();
            XMLEventReader reader = factory.createXMLEventReader(new FileInputStream("contact.xml"));
            StartElement element = null;
            Contact currentContact = null;
            while (reader.hasNext()) {
                XMLEvent nextEvent = reader.nextEvent();
                if (nextEvent.isStartElement()) {
                    element = nextEvent.asStartElement();
                    String value = reader.nextEvent().asCharacters().getData();
                    switch (element.getName().toString()) {
                    case "contact":
                        currentContact = new Contact();
                        String strId = element.getAttributeByName(new QName("id")).getValue();
                        currentContact.setId(Long.parseLong(strId));

                    case "prenom":
                        currentContact.setPrenom(value);
                        break;
                    case "nom":
                        currentContact.setNom(value);
                        break;
                    case "email":
                        currentContact.setEmail(value);
                        break;
                    case "datenaissance":
                        currentContact.setDateNaissance(LocalDate.parse(value));
                        break;
                    }
                } else if (nextEvent.isEndElement()) {
                    EndElement endElement = nextEvent.asEndElement();
                    if (endElement.getName().toString().equals("contact")) {
                        contacts.add(currentContact);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        contacts.forEach(System.out::println);
    }

}
