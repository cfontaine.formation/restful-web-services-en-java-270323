package fr.dawan.projet1xmljson.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

@XmlRootElement(name="rolodex")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name="",propOrder = {"contacts"})
public class Rolodex implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private List<Contact> contacts;
    
    public List<Contact> getContacts(){
        if(contacts==null) {
            contacts=new ArrayList<>();
        }
        return this.contacts;
    }

}
