package fr.dawan.projet1xmljson.entities;

import java.io.Serializable;
import java.time.LocalDate;

import fr.dawan.projet1xmljson.adapters.LocalDateAdapter;
import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;
import jakarta.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

//@XmlRootElement(name="contact")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "prenom", "nom", "email", "dateNaissance" })
public class Contact implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlAttribute(name = "id", required = true)
    private long id;

    @XmlElement(name = "prenom")
    private String prenom;

    @XmlElement
    private String nom;

    @XmlElement(name = "email")
    private String email;

    @XmlElement(name = "datenaissance")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate dateNaissance;

    @XmlTransient
    private String nepaspersiter;

    public Contact() {

    }

    public Contact(String prenom, String nom, String email, LocalDate dateNaissance) {
        this.prenom = prenom;
        this.nom = nom;
        this.email = email;
        this.dateNaissance = dateNaissance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(LocalDate dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @Override
    public String toString() {
        return "Contact [id=" + id + ", prenom=" + prenom + ", nom=" + nom + ", email=" + email + ", dateNaissance="
                + dateNaissance + "]";
    }

}
