package fr.dawan.projet1xmljson.entities;

import java.io.Serializable;
import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @JsonProperty(value = "ref")
    private long id;

    @JsonProperty
    private String description;

    @JsonProperty
    private double prix;

    @JsonIgnore
    private String nePasPersiter;

    @JsonProperty
    @JsonFormat(pattern = "dd-MM-yyyy")
    private LocalDate dateProduction;

    public Produit() {
    }

    public Produit(String description, double prix, LocalDate dateProduction) {
        super();
        this.description = description;
        this.prix = prix;
        this.dateProduction = dateProduction;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public LocalDate getDateProduction() {
        return dateProduction;
    }

    public void setDateProduction(LocalDate dateProduction) {
        this.dateProduction = dateProduction;
    }

    @Override
    public String toString() {
        return "Produit [id=" + id + ", description=" + description + ", prix=" + prix + ", dateProduction="
                + dateProduction + "]";
    }
}
