package fr.dawan.projet1xmljson.entities;

import jakarta.xml.bind.annotation.XmlRegistry;

@XmlRegistry
public class ObjectFactory {

    public Contact createContact() {
        return new Contact();
    }

    public Rolodex createRolodex() {
        return new Rolodex();
    }

}
