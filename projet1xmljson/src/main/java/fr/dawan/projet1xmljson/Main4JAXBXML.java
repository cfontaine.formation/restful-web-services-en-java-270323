package fr.dawan.projet1xmljson;

import java.io.FileReader;
import java.io.FileWriter;
import java.time.LocalDate;

import fr.dawan.projet1xmljson.entities.Contact;
import fr.dawan.projet1xmljson.entities.ObjectFactory;
import fr.dawan.projet1xmljson.entities.Rolodex;
import fr.dawan.projet1xmljson.tools.XmlTools;

public class Main4JAXBXML {

    public static void main(String[] args) {
        Contact c = new Contact("Alan", "Smithee", "a@dawan.com", LocalDate.of(1993, 10, 5));
        c.setId(1L);
//        
//        try(FileWriter fw=new FileWriter("contact3.xml")){
//            XmlTools.toXml(fw, c, Contact.class,ObjectFactory.class);
//        }
//        catch(Exception e) {
//            e.printStackTrace();
//        }
//        
//        try(FileReader fr=new FileReader("contact3.xml")){
//            Contact c2=XmlTools.fromXml(fr,  Contact.class,ObjectFactory.class);
//            System.out.println(c2);
//        }      catch(Exception e) {
//            e.printStackTrace();
//        }

        Rolodex rolodex = new Rolodex();
        rolodex.getContacts().add(c);
        c = new Contact("John", "Doe", "jd@dawan.com", LocalDate.of(2003, 11, 5));
        c.setId(2L);
        rolodex.getContacts().add(c);
        c = new Contact("Jane", "Doe", "jane.doe@dawan.com", LocalDate.of(1999, 1, 15));
        c.setId(3L);
        rolodex.getContacts().add(c);
        c = new Contact("Yves", "Roulleau", "yvesr@dawan.com", LocalDate.of(1991, 11, 6));
        c.setId(4L);
        rolodex.getContacts().add(c);

        try (FileWriter fw = new FileWriter("rolodex.xml")) {
            XmlTools.toXml(fw, rolodex, Contact.class, Rolodex.class, ObjectFactory.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (FileReader fr = new FileReader("rolodex.xml")) {
            Rolodex r2 = XmlTools.fromXml(fr, Contact.class, Rolodex.class, ObjectFactory.class);
            r2.getContacts().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
