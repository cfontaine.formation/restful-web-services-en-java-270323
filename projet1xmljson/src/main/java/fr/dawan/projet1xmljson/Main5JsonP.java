package fr.dawan.projet1xmljson;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.time.LocalDate;

import fr.dawan.projet1xmljson.entities.Contact;
import fr.dawan.projet1xmljson.entities.Rolodex;
import jakarta.json.Json;
import jakarta.json.JsonArray;
import jakarta.json.JsonArrayBuilder;
import jakarta.json.JsonObject;
import jakarta.json.JsonObjectBuilder;
import jakarta.json.JsonReader;
import jakarta.json.JsonWriter;

public class Main5JsonP {

    public static void main(String[] args) {
        Contact c = new Contact("Alan", "Smithee", "a@dawan.com", LocalDate.of(1993, 10, 5));
        c.setId(1L);
        Rolodex rolodex = new Rolodex();
        rolodex.getContacts().add(c);
        c = new Contact("John", "Doe", "jd@dawan.com", LocalDate.of(2003, 11, 5));
        c.setId(2L);
        rolodex.getContacts().add(c);
        c = new Contact("Jane", "Doe", "jane.doe@dawan.com", LocalDate.of(1999, 1, 15));
        c.setId(3L);
        rolodex.getContacts().add(c);
        c = new Contact("Yves", "Roulleau", "yvesr@dawan.com", LocalDate.of(1991, 11, 6));
        c.setId(4L);
        rolodex.getContacts().add(c);

        try {
            jsonWrite(c, "contact.json");
            Contact c2 = JsonReaderFile("contact.json");
            System.out.println(c2);

            jsonWriteRolodex(rolodex, "rolodex.json");
            Rolodex r2 = JsonReaderFileRolodex("rolodex.json");
            r2.getContacts().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void jsonWrite(Contact c, String path) throws Exception {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        builder.add("id", c.getId()).add("prenom", c.getPrenom()).add("nom", c.getNom()).add("email", c.getEmail())
                .add("dateNaissance", c.getDateNaissance().toString());

        JsonObject contactJsonObject = builder.build();
        OutputStream os = new FileOutputStream(path);
        JsonWriter jsonWriter = Json.createWriter(os);
        jsonWriter.write(contactJsonObject);
        jsonWriter.close();
        os.close();
    }

    public static Contact JsonReaderFile(String path) throws Exception {
        InputStream fi = new FileInputStream(path);
        JsonReader reader = Json.createReader(fi);
        JsonObject jsonObject = reader.readObject();
        reader.close();
        fi.close();
        Contact c = new Contact(jsonObject.getString("prenom"), jsonObject.getString("nom"),
                jsonObject.getString("email"), LocalDate.parse(jsonObject.getString("dateNaissance")));
        c.setId(jsonObject.getInt("id"));
        return c;
    }

    public static void jsonWriteRolodex(Rolodex r, String path) throws Exception {
        JsonObjectBuilder builder = Json.createObjectBuilder();
        JsonArrayBuilder rolodexBuilder = Json.createArrayBuilder();

        for (Contact c : r.getContacts()) {
            builder.add("id", c.getId()).add("prenom", c.getPrenom()).add("nom", c.getNom()).add("email", c.getEmail())
                    .add("dateNaissance", c.getDateNaissance().toString());
            JsonObject contactJsonObject = builder.build();

            rolodexBuilder.add(contactJsonObject);
        }
        JsonArray jsonArray = rolodexBuilder.build();
        OutputStream os = new FileOutputStream(path);
        JsonWriter jsonWriter = Json.createWriter(os);
        jsonWriter.write(jsonArray);
        jsonWriter.close();
        os.close();
    }

    public static Rolodex JsonReaderFileRolodex(String path) throws Exception {
        InputStream fi = new FileInputStream(path);
        JsonReader reader = Json.createReader(fi);
        JsonArray jsonArray = reader.readArray();
        reader.close();
        fi.close();
        Rolodex r = new Rolodex();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.getJsonObject(i);
            Contact c = new Contact(jsonObject.getString("prenom"), jsonObject.getString("nom"),
                    jsonObject.getString("email"), LocalDate.parse(jsonObject.getString("dateNaissance")));
            c.setId(jsonObject.getInt("id"));
            r.getContacts().add(c);
        }
        return r;
    }

}
