package fr.dawan.projet1xmljson;

import java.io.File;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import fr.dawan.projet1xmljson.handlers.ContactHandler;

public class Main2SaxXML {

    public static void main(String[] args) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser parser = factory.newSAXParser();
            ContactHandler handler = new ContactHandler();
            parser.parse(new File("contact.xml"), handler);
            handler.getContactList().forEach(System.out::println);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
