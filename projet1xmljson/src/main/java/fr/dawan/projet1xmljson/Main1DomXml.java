package fr.dawan.projet1xmljson;

import fr.dawan.projet1xmljson.tools.ContactDomTools;

public class Main1DomXml {

    public static void main(String[] args) {
        ContactDomTools d = new ContactDomTools();
        try {
            d.loadXmlFile("contact.xml");
            d.findAll().forEach(System.out::println);
            d.remove(2L);
            d.findAll().forEach(System.out::println);
            d.writeXMLFile("contact2.xml");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
