package fr.dawan.projet1xmljson.tools;

import java.io.FileWriter;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.dawan.projet1xmljson.entities.Contact;

public class ContactDomTools {

    private Document doc;

    public void loadXmlFile(String path) throws Exception {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = dbFactory.newDocumentBuilder();
        doc = builder.parse(path);
        doc.getDocumentElement().normalize();
    }

    public void writeXMLFile(String path) throws Exception {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult(new FileWriter(path));
        DOMSource source = new DOMSource(doc);
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.transform(source, result);
    }

    public List<Contact> findAll() {
        NodeList nodeLst = doc.getElementsByTagName("contact");
        List<Contact> contacts = new ArrayList<>();
        for (int i = 0; i < nodeLst.getLength(); i++) {
            if (nodeLst.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) nodeLst.item(i);
                Contact c = new Contact();
                c.setPrenom(getElementValue(elm, "prenom"));
                c.setNom(getElementValue(elm, "nom"));
                c.setEmail(getElementValue(elm, "email"));
                c.setDateNaissance(LocalDate.parse(getElementValue(elm, "datenaissance")));
                c.setId(Long.parseLong(elm.getAttribute("id")));
                contacts.add(c);
            }
        }
        return contacts;
    }

    public void remove(long id) {
        String strId = Long.toString(id);
        NodeList nodeLst = doc.getElementsByTagName("contact");
        for (int i = 0; i < nodeLst.getLength(); i++) {
            if (nodeLst.item(i).getNodeType() == Node.ELEMENT_NODE) {
                Element elm = (Element) nodeLst.item(i);
                if (elm.getAttribute("id").equals(strId)) {
                    doc.getDocumentElement().removeChild(elm);
                    return;
                }
            }
        }
    }

    private String getElementValue(Element element, String name) {
        NodeList nl = element.getElementsByTagName(name).item(0).getChildNodes();
        return nl.item(0).getNodeValue();
    }

}
