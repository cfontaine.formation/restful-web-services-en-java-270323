package fr.dawan.projet1xmljson.tools;

import java.io.FileReader;
import java.io.FileWriter;

import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;

public class XmlTools {

    public static <T> void toXml(FileWriter fw, T obj, Class<?>... classesToBeBound) throws Exception {
        JAXBContext context = JAXBContext.newInstance(classesToBeBound);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        m.marshal(obj, fw);
    }

    public static <T> T fromXml(FileReader fr, Class<?>... classesToBeBound) throws Exception {
        JAXBContext context = JAXBContext.newInstance(classesToBeBound);
        Unmarshaller m = context.createUnmarshaller();
        Object obj = m.unmarshal(fr);
        return (T) obj;
    }
}
