package fr.dawan.projet1xmljson.tools;

import java.io.FileReader;
import java.io.FileWriter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

public class JsonTools {

    public static <T> void toJson(FileWriter fw, T obj) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.writeValue(fw, obj);
    }

    public static <T> T fromJson(FileReader fr, Class<T> classType) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        return mapper.readValue(fr, classType);
    }

}
