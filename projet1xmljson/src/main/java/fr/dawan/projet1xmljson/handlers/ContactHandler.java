package fr.dawan.projet1xmljson.handlers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import fr.dawan.projet1xmljson.entities.Contact;

public class ContactHandler extends DefaultHandler {

    private List<Contact> contactList = new ArrayList<>();

    private Stack<String> elementStack = new Stack<>();

    private Stack<Contact> contactStack = new Stack<>();

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        elementStack.push(qName);
        if (qName.equals("contact")) {
            Contact c = new Contact();
            c.setId(Long.parseLong(attributes.getValue(0)));
            contactStack.push(c);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        elementStack.pop();
        if ("contact".equals(qName)) {
            contactList.add(contactStack.pop());
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length).trim();

        if (value.length() == 0) {
            return;
        }

        String currentElement = elementStack.peek();
        Contact currentContact = contactStack.peek();
        switch (currentElement) {
        case "prenom":
            currentContact.setPrenom(value);
            break;
        case "nom":
            currentContact.setNom(value);
            break;
        case "email":
            currentContact.setEmail(value);
            break;
        case "datenaissance":
            currentContact.setDateNaissance(LocalDate.parse(value));
            break;
        }

    }

    public List<Contact> getContactList() {
        return contactList;
    }

}
