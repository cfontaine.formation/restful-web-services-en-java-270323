package fr.dawan.projet5springjwt.security;

import java.util.Date;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;

@Component
public class JwtTokenUtil {

    @Value("${jwt.secret}")
    private String secret;
    
    @Value("${jwt.token.validity}")
    private int validity;
    
    
    public String createToken(String username) {
        Date now = new Date();
        Algorithm algorithm = Algorithm.HMAC512(secret);
        return JWT.create()
                .withIssuer(username)
                .withIssuedAt(now)
                .withExpiresAt(new Date(now.getTime()+validity))
                .sign(algorithm);
    }
    
    public  String getUsernameFromToken(String token) {
        return JWT.require(Algorithm.HMAC512(secret.getBytes()))
                .build()
                .verify(token)
                .getSubject();
    }
    
    boolean validateToken(String token) {
        Algorithm algorithm = Algorithm.HMAC512(secret);
        try {
            JWT.require(algorithm)
                    .build()
                    .verify(token);
            return true;
        } catch (JWTVerificationException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

}
