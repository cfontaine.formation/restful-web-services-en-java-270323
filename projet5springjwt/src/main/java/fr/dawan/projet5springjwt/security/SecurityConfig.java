package fr.dawan.projet5springjwt.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

import fr.dawan.projet5springjwt.controllers.AppAuthProvider;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

    @Autowired
    private JwtRequestFilter filter;

//    @Autowired
//    private UserDetailsService userService;
    
    @Autowired
    private JwtAuthenticationEntryPoint authenticationEntryPoint;

    @Bean
    PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
    
//    @Bean
//    AuthenticationProvider provider() {
//        AppAuthProvider provider=new AppAuthProvider();
//        provider.setUserDetailsService(userService);
//        return provider;
//    }
    
    @Bean
    SecurityFilterChain filterChain(HttpSecurity http, AuthenticationProvider provider) throws Exception {
        http
        .exceptionHandling().authenticationEntryPoint(authenticationEntryPoint)
        .and()
        .addFilterBefore(filter, JwtRequestFilter.class).csrf(csrf -> csrf.disable()).sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        .authenticationProvider(provider)
        .authorizeHttpRequests((requests) -> requests.requestMatchers(HttpMethod.POST, "/login")
        .permitAll()
        .anyRequest().authenticated());
        return http.build();
    }

}
