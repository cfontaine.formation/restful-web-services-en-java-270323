package fr.dawan.projet5springjwt.controllers;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import fr.dawan.projet5springjwt.entities.Article;
import fr.dawan.projet5springjwt.repositories.ArticleRepository;

@RestController
@RequestMapping("/api/articles")
public class ArticleController {

    @Autowired
    private ArticleRepository repositoryArticle;

    @ResponseStatus(code = HttpStatus.I_AM_A_TEAPOT)
    @GetMapping(produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    public List<Article> getAllArticles() {
        return repositoryArticle.findAll();
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE, params = { "page", "size" })
    public Page<Article> getAllArticlesPage(Pageable page) {
        return repositoryArticle.findAll(page);
    }

    @GetMapping(value = "/{id:[0-9]+}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Article getArticleById(
             @PathVariable long id) {
        return repositoryArticle.findById(id).get();
    }

    @GetMapping(value = "/{description:^[a-zA-Z][a-zA-Z0-9 ]+$}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Article> getArticleByDescription(@PathVariable String description) {
        return repositoryArticle.findByDescriptionLikeIgnoreCase("%" + description + "%");
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.TEXT_PLAIN_VALUE)
    public ResponseEntity<String> deleteArticle(@PathVariable long id) {
        try {
            repositoryArticle.deleteById(id);
            return new ResponseEntity<>("L'article id=" + id + "est supprimé", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>("L'article n'existe pas", HttpStatus.NOT_FOUND);
        }

    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Article addArticle(@RequestBody Article article) {
        return repositoryArticle.saveAndFlush(article);
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Article updateArticle(@PathVariable long id, @RequestBody Article article) {
        article.setId(id);
        return repositoryArticle.saveAndFlush(article);
    }

    @GetMapping("exception/io")
    public void genIOException() throws IOException {
        throw new IOException("Le traitement a échoué");
    }

    @GetMapping("exception/sql")
    public void genSqlException() throws IOException, SQLException {
        throw new SQLException("La requete a échoué");
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<String> gestionIoException(IOException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
