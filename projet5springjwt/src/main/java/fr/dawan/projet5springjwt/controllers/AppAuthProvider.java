package fr.dawan.projet5springjwt.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AppAuthProvider extends DaoAuthenticationProvider {
    
    @Autowired
    private UserDetailsService userService;
    
    @Autowired
    private PasswordEncoder encoder;
    
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UsernamePasswordAuthenticationToken auth=(UsernamePasswordAuthenticationToken) authentication;
        String username=auth.getName();
        String password=auth.getCredentials().toString();
        UserDetails userDb=userService.loadUserByUsername(username);
        if(password!=null && encoder.matches(password, userDb.getPassword())) {
           return new UsernamePasswordAuthenticationToken(userDb,null,null);
        }
        else {
            throw new BadCredentialsException("Mauvais mot de passe");
        }
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }

    
}
