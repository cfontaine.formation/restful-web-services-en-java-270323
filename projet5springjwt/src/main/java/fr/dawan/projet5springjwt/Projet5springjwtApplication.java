package fr.dawan.projet5springjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Projet5springjwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(Projet5springjwtApplication.class, args);
	}

}
