package fr.dawan.projet5springjwt.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.dawan.projet5springjwt.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    List<Article> findByDescriptionLikeIgnoreCase(String model);

}
