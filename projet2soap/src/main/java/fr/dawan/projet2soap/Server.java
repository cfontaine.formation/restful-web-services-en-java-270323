package fr.dawan.projet2soap;

import fr.dawan.projet2soap.ws.Calculatrice;
import jakarta.xml.ws.Endpoint;

public class Server {

    public static void main(String[] args) {
        String adresse = "http://localhost:8080/calculatrice";
        Endpoint.publish(adresse, new Calculatrice());
        System.out.println("WSDL: http://localhost:8080/calculatrice?wsdl");
        System.out.println("XSD: http://localhost:8080/calculatrice?xsd=1");
    }

}
