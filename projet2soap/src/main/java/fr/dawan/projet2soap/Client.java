package fr.dawan.projet2soap;

import fr.dawan.projet2soap.client.Calculatrice;
import fr.dawan.projet2soap.client.CalculatriceService;

public class Client {

    public static void main(String[] args) {
        CalculatriceService service = new CalculatriceService();
        Calculatrice calculatrice = service.getCalculatricePort();
        System.out.println(calculatrice.additionner(4, 5));
        calculatrice.listeProduit().forEach(System.out::println);
    }

}
