package fr.dawan.projet2soap.ws;

import java.util.ArrayList;
import java.util.List;

import fr.dawan.projet2soap.entities.Produit;
import jakarta.jws.WebMethod;
import jakarta.jws.WebParam;
import jakarta.jws.WebResult;
import jakarta.jws.WebService;

@WebService
public class Calculatrice {

    @WebMethod(operationName = "additionner")
    public @WebResult(name = "resultat") int addition(@WebParam(name = "a") int a, @WebParam(name = "b") int b) {
        return a + b;
    }

    @WebMethod
    public List<Produit> listeProduit() {
        List<Produit> produits = new ArrayList<>();
        Produit p = new Produit("Tv 4K", 700);
        p.setId(1L);
        produits.add(p);
        p = new Produit("Souris", 30);
        p.setId(2L);
        produits.add(p);
        return produits;
    }

}
