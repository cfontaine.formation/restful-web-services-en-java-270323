
package fr.dawan.projet2soap.client;

import javax.xml.namespace.QName;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.bind.annotation.XmlElementDecl;
import jakarta.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.dawan.projet2soap.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private static final QName _Additionner_QNAME = new QName("http://ws.projet2soap.dawan.fr/", "additionner");
    private static final QName _AdditionnerResponse_QNAME = new QName("http://ws.projet2soap.dawan.fr/", "additionnerResponse");
    private static final QName _ListeProduit_QNAME = new QName("http://ws.projet2soap.dawan.fr/", "listeProduit");
    private static final QName _ListeProduitResponse_QNAME = new QName("http://ws.projet2soap.dawan.fr/", "listeProduitResponse");
    private static final QName _Produit_QNAME = new QName("http://ws.projet2soap.dawan.fr/", "produit");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.dawan.projet2soap.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Additionner }
     * 
     * @return
     *     the new instance of {@link Additionner }
     */
    public Additionner createAdditionner() {
        return new Additionner();
    }

    /**
     * Create an instance of {@link AdditionnerResponse }
     * 
     * @return
     *     the new instance of {@link AdditionnerResponse }
     */
    public AdditionnerResponse createAdditionnerResponse() {
        return new AdditionnerResponse();
    }

    /**
     * Create an instance of {@link ListeProduit }
     * 
     * @return
     *     the new instance of {@link ListeProduit }
     */
    public ListeProduit createListeProduit() {
        return new ListeProduit();
    }

    /**
     * Create an instance of {@link ListeProduitResponse }
     * 
     * @return
     *     the new instance of {@link ListeProduitResponse }
     */
    public ListeProduitResponse createListeProduitResponse() {
        return new ListeProduitResponse();
    }

    /**
     * Create an instance of {@link Produit }
     * 
     * @return
     *     the new instance of {@link Produit }
     */
    public Produit createProduit() {
        return new Produit();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Additionner }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Additionner }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.projet2soap.dawan.fr/", name = "additionner")
    public JAXBElement<Additionner> createAdditionner(Additionner value) {
        return new JAXBElement<>(_Additionner_QNAME, Additionner.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionnerResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AdditionnerResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.projet2soap.dawan.fr/", name = "additionnerResponse")
    public JAXBElement<AdditionnerResponse> createAdditionnerResponse(AdditionnerResponse value) {
        return new JAXBElement<>(_AdditionnerResponse_QNAME, AdditionnerResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListeProduit }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListeProduit }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.projet2soap.dawan.fr/", name = "listeProduit")
    public JAXBElement<ListeProduit> createListeProduit(ListeProduit value) {
        return new JAXBElement<>(_ListeProduit_QNAME, ListeProduit.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListeProduitResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ListeProduitResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.projet2soap.dawan.fr/", name = "listeProduitResponse")
    public JAXBElement<ListeProduitResponse> createListeProduitResponse(ListeProduitResponse value) {
        return new JAXBElement<>(_ListeProduitResponse_QNAME, ListeProduitResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Produit }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Produit }{@code >}
     */
    @XmlElementDecl(namespace = "http://ws.projet2soap.dawan.fr/", name = "produit")
    public JAXBElement<Produit> createProduit(Produit value) {
        return new JAXBElement<>(_Produit_QNAME, Produit.class, null, value);
    }

}
