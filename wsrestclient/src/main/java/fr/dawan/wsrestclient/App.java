package fr.dawan.wsrestclient;

import java.time.LocalDate;

import fr.dawan.wsrestclient.entities.Article;
import jakarta.ws.rs.client.Client;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.Invocation;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class App {
    public static void main(String[] args) {
        Client client = ClientBuilder.newClient();
        WebTarget wt = client.target("http://localhost:8080/wsrest/api/articles");
        Invocation.Builder ib = wt.request(MediaType.APPLICATION_JSON);
        Response response = ib.get();
        System.out.println(response.getStatus());
        System.out.println(response.readEntity(String.class));

        Article a = new Article("Clavier", 39.0, LocalDate.of(2020, 1, 10));
        createArticle(a);
    }

    private static void createArticle(Article a) {
        Client client=ClientBuilder.newClient();
        WebTarget wt=client.target("http://localhost:8080/wsrest/api/articles");
        Invocation.Builder ib=wt.request(MediaType.APPLICATION_JSON);
        Response r=ib.post(Entity.entity(a, MediaType.APPLICATION_JSON));
        System.out.println(r.getStatus());
        System.out.println(r.readEntity(String.class));
        
    }
}